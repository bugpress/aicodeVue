const Koa = require('koa')
const app = new Koa()
const Router = require('koa-router')
let router = new Router()

const mongoose = require('mongoose')
const fs = require('fs')
//商品详情表
router.get('/insertAllGoodsInfo',async(ctx)=>{
    fs.readFile('data_json/newgoods.json','utf8',(err,data)=>{
        //换成对象
        data = JSON.parse(data)
        let saveCount = 0
        //插入的表
        const Goods = mongoose.model('Goods')
        data.map((value,index)=>{
            console.log(value)
            let newGoods = new Goods(value)
            newGoods.save().then(()=>{
                saveCount++
                console.log('成功'+saveCount)
            }).catch(error=>{
                console.log('失败：'+error)
            })
        })
    })
    //开始导入数据
   // ctx.body = "开始导入数据"
})

//商品类别表
router.get('/insertAllCategory',async(ctx)=>{
    fs.readFile('data_json/category.json','utf8',(err,data)=>{
        data=JSON.parse(data)
        let saveCount=0
        const Category = mongoose.model('Category')
        data.RECORDS.map((value,index)=>{
            console.log(value)
            let newCategory = new Category(value)
            newCategory.save().then(()=>{
                saveCount++
                console.log('成功'+saveCount)
            }).catch(error=>{
                 console.log('失败：'+error)
            })
        })
       
        
    })
    ctx.body="开始导入数据"
 
})

//商品子类
router.get('/insertAllCategorySub',async(ctx)=>{
    fs.readFile('data_json/categorySub.json','utf8',(err,data)=>{
        data = JSON.parse(data)
        let saveCount = 0
        const CategorySub = mongoose.model('CategorySub')
        data.RECORDS.map((value,index)=>{
            console.log(value)
            let newCategorySub = new CategorySub(value)
            newCategorySub.save().then(()=>{
                saveCount++
                console.log('成功'+saveCount)
            }).catch(error=>{
                console.log('失败：'+error)
            })
        })

    })
    ctx.body = "导入商品子类数据"
})

//获取商品详情信息接口
//用findeOne的形式查找出一条商品数据
router.post('/getDetailGoodsInfo',async(ctx)=>{
    try{
        let goodsId = ctx.request.body.goodsId
        const Goods = mongoose.model('Goods')
        console.log(goodsId)
        let result= await Goods.findOne({ID:goodsId}).exec()
        ctx.body={code:200,message:result}
    }catch(error){
        ctx.body={code:500,message:error}
    }
})


//分类页面的数据读取
router.get('/getCategoryList',async(ctx)=>{
    try{
        const Category = mongoose.model('Category')
        let result = await Category.find().exec()
        ctx.body = {code:200,message:result}
    }catch(err){
        ctx.body={code:500,message:err}
    }
})


//读取小类
router.post('/getCategorySubList',async(ctx)=>{
    try{
        let categoryId = ctx.request.body.categoryId
        const CategorySub = mongoose.model('CategorySub')
        let result = await CategorySub.find({MALL_CATEGORY_ID:categoryId}).exec()
        ctx.body = {code:200,message:result}
    }catch(err){
        ctx.body = {code:500,message:err}
    }
})

//根据商品类别获取商品列表
router.post('/getGoodsListByCategorySubID',async(ctx)=>{
    try{
        //小类别,从前台获取categorySubId和当前页数
        let categorySubId = ctx.request.body.categorySubId
        //分页制作
        let page = ctx.request.body.page
        //每行显示的数量
        let num = 10
        let start = (page-1)*num
        const Goods = mongoose.model('Goods')
        let result = await Goods.find({SUB_ID:categorySubId}).skip(start).limit(num).exec()
        ctx.body={code:200,message:result}
    }catch(err){
        ctx.body={code:500,message:err}
    }
})

module.exports = router