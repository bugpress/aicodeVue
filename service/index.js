const Koa = require('koa')
const app = new Koa()
//引入connect
const {connect , initSchemas} = require('./database/init.js')
const mongoose = require('mongoose')
const Router = require('koa-router')
const bodyParser = require('koa-bodyparser')
app.use(bodyParser())
const cors = require('koa2-cors')
app.use(cors())

//引入我们的user.js模块
let user = require('./appApi/user.js')
let goods = require('./appApi/goods.js')
//装载所有子路由
let router = new Router();
router.use('/user',user.routes())
router.use('/goods',goods.routes())
//加载路由中间件
app.use(router.routes())
app.use(router.allowedMethods())

//立即执行函数
;(async () =>{
    await connect()
    initSchemas()
})()

app.use(async(ctx)=>{
    ctx.body='<h1>hello Hedy</h2>'
})

app.listen(3000,()=>{
    console.log('[Server] starting at port 3000')
})
 