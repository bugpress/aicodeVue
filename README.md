# aicode

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).



个人解释说明：
1.初始化项目之后，安装Vant：npm install vant --save，引入：npm install babel-plugin-import --save-dev，在.babelrc中配置plugins（插件）
"plugins": [
    "transform-vue-jsx", 
    "transform-runtime",
    ["import",{"libraryName":"vant","style":true}]
  ]
之后再main.js中引用需要的组件：
import {Button} from 'vant' 
Vue.use(Button)
2.移动端的适配器:移动端用Rem + JavaScript完成屏幕适配的方法
     <script>
      //得到手机屏幕的宽度
        let htmlWidth = document.documentElement.clientWidth || document.body.clientWidth;
        //得到html的Dom元素
        let htmlDom = document.getElementsByTagName('html')[0];
        //设置根元素字体大小
        htmlDom.style.fontSize= htmlWidth/20 + 'px';    
    </script>

3.首页布局和路由设置:meta标签进行设置，加入user-scalable=no不能缩放页面大小
4.首页轮播图：加载Swipe组件,利用Vant实现图片轮播的懒加载->Lazyload 
5.easyMock（生成模拟数据）和Axios的使用:npm install --save axios
6.首页商品分类栏的布局：flex布局
7.广告banner的布局,改造swpie组件
8.商品推荐vue-awesome-swiper:npm install vue-awesome-swiper --save，重要！！！
   ①vue-awesome-swiper详解，封装好后，可以引用到需要的页面：import swiperDefault from '../swiper/swiperDefault'，然后注册组件：components:{swiper,swiperSlide,swiperDefault},注册好后直接在<template></template>使用：<swiperDefault></swiperDefault>
    ②添加分页器,需要在swiper标签上使用options属性来进行配置:在data里面设置：swiperOption:{
      pagination:{
          el:'.swiper-pagination'
      }
    }
    然后在template标签里加入一个div用于显示分页器,注意的是要在swiper-slide外层。<div class="swiper-pagination" slot="pagination"></div>最后是在swiper标签里加入 :options="swiperOption"。就实现了有分页期的效果。

9.首页楼层区域布局:组件的封装
10.过滤器：针对小数,@代表的是src目录的意思，这个是webpack的配置，我们可以在/build/webpack.base.conf.js里找到这个配置项
11.首页热卖模块的van-list组件使用
12.编写后端服务接口配置文件
13.安装koa2
   npm init -y初始化
   npm install --save koa安装
   安装MongoDB
   完成之后进行连接，再执行npm install mongoose --save 安装mongoose
   编写数据库连接配置init.js
14.数据库连接增加Promise支持：我们在作这个init.js文件时，必须确保先连接数据库后，再作其他事情，所以我们需要在所有代码的外层增加一个Promise。return  new Promise((resolve,reject)=>{
//把所有连接放到这里
})

15. Mongoose的Schema建模介绍:Schema相当于MongoDB数据库的一个映射,Schema是以key-value形式Json格式的数据,Mongoose中的三个概念
schema ：用来定义表的模版，实现和MongoDB数据库的映射。用来实现每个字段的类型，长度，映射的字段，不具备表的操作能力。
model ：具备某张表操作能力的一个集合，是mongoose的核心能力。我们说的模型就是这个Mondel。
entity ：类似记录，由Model创建的实体，也具有影响数据库的操作能力。
步骤：创建Schema->载入Schema和插入查出数据（载入所有Schema
直接在service\init.js 先引入一个glob和一个resolve）glob：node的glob模块允许你使用 * 等符号，来写一个glob规则，像在shell里一样，获取匹配对应规则文件。
resolve: 将一系列路径或路径段解析为绝对路径。
16.安全的用户密码加密机制:npm install --save bcrypt --registry=https://registry.npm.taobao.org
17.注册页面的vue模板编写
18.Koa2的用户操作的路由模块化:安装koa-router(npm install koa-router --save)
19.注册用户的前后端通讯:安装koa-bodyparser中间件(npm install --save koa-bodyparser),koa2支持跨域请求：安装koa2-cors中间件，在koa2里解决跨域的中间件叫koa2-cors，进行安装npm install --save koa2-cors,注意bodyParser用法错误的用法const bodyParser = require('koa-bodyparser')
app.use(bodyParser())或者app.use(bodyParser)

20.用户注册数据库操作：Koa2的User.js 接口的完善，引入数据库，前端的业务逻辑判断
21.注册的防重复提交:按钮上绑定loading属性

22.注册时的前端验证:写前端验证，重写注册方法和绑定按钮事件

23.vue的登录界面制作和路由的配置和登录接口的编写

24.登录的前端交互效果制作和登录状态存储

25.商品详细数据的提纯操作：重点node的fs模块

26.批量插入商品详情数据到MongoDB中

27.商品大类的Shema建立和导入数据库

28.批量导入数据完毕

29.编写商品接口

30.完善有关商品的页面以及跳转

31.商品详情的页面模板编写 

32.补充商品详细页的滑动切换和吸顶效果：通过van-tabs里的swipeable属性就可以开启滑动切换tab页的效果，通过van-tabs里的sticky 属性可以开启吸顶效果，也叫粘性布局，当Tab滚动到顶部时会自动吸顶。

33.商品列表页,获取数据和交互效果,以及一级栏目和二级栏目的联动

34.商品列表页上拉加载效果的实现

35.商品列表页下拉刷新效果的实现,引入Vant中的PullRefresh组件

36.商品类别分类的Koa2分页服务

37.商品数据上拉加载效果和商品详细数据的获取

38.真实数据的下拉刷新效果

39.Vue中图片失效替补图片的制作方法

40.商品列表页编程式导航的制作

知识点：params和query传参的用法
params传参，路径不能使用path，只能使用name，不然取不到传的数据：this.$router.push({name:'Goods',params:{goodsId:id}})
取数据时用params获取：this.$route.params.goodsId

query传参，用的是path，而不是name,否则也会出错：this.$router.push({path:'/Goods',query:{goodsId:id}})
数据使用query：this.$route.query.goodsId

41.购物车页面的的建立:主要知识点在H5新增localStorage本地存储
进入页面要作的第一件事就是取得localStorage里的数据，我们先在data里注册两个属性cartInfo(购物车中商品的信息)和isEmpty（购物是否为空的标识，方便页面呈现）,然后再编写具体的getCartInfo()方法
 
42.购物车中商品的添加

43.显示购物车，清空购物车




