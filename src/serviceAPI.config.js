const BASEURL = "https://www.easy-mock.com/mock/5b592190e4e04f38c7a558b9/aicode/"
const LOCALURL = "http://localhost:3000/"
const URL = {
    getShoppingMallInfo:BASEURL+'index',
    getGoodsInfo:BASEURL+'getGoodsInfo',
    //用户注册接口
    registerUser:LOCALURL+'user/register',
    //用户登录接口
    login : LOCALURL+'user/login', 
    //商品详情接口
    getDetailGoodsInfo : LOCALURL+'goods/getDetailGoodsInfo',
    //得到大类信息 
    getCateGoryList : LOCALURL+'goods/getCateGoryList',  
    //读取小类
    getCategorySubList : LOCALURL+'goods/getCategorySubList',
    //根据商品类别获取商品列表
    getGoodsListByCategorySubID : LOCALURL+'goods/getGoodsListByCategorySubID',
    

}

module.exports = URL